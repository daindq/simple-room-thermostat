;=================================Du an mon hoc VXL========================
;Nguoi tong hop: Nguyen Dinh Quoc Dai 20191724
	org 00h
	nop
	sjmp main
	
	org 03h ;Ngat Ex0
	nop
	ljmp SetButtonPressed
	
	org 0bh	;Ngat Timer0
	nop
	ljmp NgatTimer0
	
	org 13h	;Ngat Ex1
	nop
	ljmp StopButtonPressed
	
	org 1bh	;Ngat Timer1
	nop
	reti
	
	org 23h	;Ngat serial
	nop
	reti
;=====================Khai bao bien, hang so========================
	adc_rd bit P1.6 
	adc_wr bit P1.5
	adc_cs bit P1.7
	adc_intr bit P1.4
	adc_port equ P2
	
	led_7 equ P0
	led_don_vi bit P1.2
	led_chuc bit P1.1
	led_tram bit P1.0
	setbutton bit P3.2
	stopbutton bit P3.3
	buzzer bit P1.3
	
	;Local variables: Registers and bits
	BCD_donvi equ R7 ;thanh ghi chua ma BCD cua hang don vi
	BCD_chuc equ R6 ;thanh ghi chua ma BCD cua hang chuc
	BCD_tram equ R5 ;thanh ghi chua ma BCD cua hang tram
	
	timer_count equ 32H ;timer count for adc 1s period
	adc_val equ 30H ;local variable for storing temp value
	temp_thrshld equ 31H ; Temperature threshold storing address
	over_temp bit 00H ; ...
	stop_mode bit 01H ; Bit for stop mode
	set_mode bit 02H ; Bit for set mode
;===================================================================
;           main program
;===================================================================
org 50h
main:
	;I/O pins initialization
	mov A, #0FFh
	mov adc_port, A
	setb setbutton
	setb stopbutton
	
	clr adc_rd
	clr adc_wr
	clr adc_cs
	clr adc_intr
	mov led_7, #00h
	clr led_don_vi
	clr led_chuc
	clr led_tram
	clr buzzer
	;Default value initialize
	setb set_mode
	clr stop_mode
	setb buzzer
	mov temp_thrshld, #05Fh ;for testing
	clr over_temp
	mov timer_count, #000H
	

	; Interupt set up ( for stop and set button + adc T = 1s sampling period)
	acall IntrSetUp
	
	;Set temperature threshold
	settemp:
		lcall DieuChinhSP
		jnb set_mode, loop1 ;re nhanh sang hien thi nhiet do neu nut set duoc bam
		sjmp settemp
	
	
	loop1:
		clr over_temp
		;acall DoNhietDo
		mov A,adc_val
		;re nhanh neu T>SP
		cjne A, temp_thrshld, next1
		sjmp norm_disp
		next1:
		jnc loop2		
		;-----------------
		norm_disp:
		lcall Hex2BCD
		lcall HienThi
		;re nhanh neu kich hoat set
		jb set_mode, settemp
		;delay ???
		sjmp loop1
	
	loop2:
		setb over_temp
		clr stop_mode
		;Hien thi co nhay
		
		lcall HienThiNhay
		clr buzzer
		;-------------------
		;acall DoNhietDo
		mov A,adc_val
		;kiem tra T>SP? Khong thi re nhanh
		cjne A, temp_thrshld, next2
		next2:
		jc stopcheck	
		;-------------------------------
		sjmp loop2
	;Da bam nut Stop chua, chua thi quay lai loop2
	stopcheck:
	jnb stop_mode, loop2
	;tat coi...
	setb buzzer
	sjmp norm_disp
		
;==========================Cac chuong trinh con======================
IntrSetUp:
	setb EA
	setb EX0
	setb EX1
	setb IT0
	setb IT1
	;set up timer
	setb ET0
	mov TMOD, #001h
	mov TL0, #0AFh
	mov TH0, #03Ch
	setb TR0
ret
DoNhietDo:
; Bien/(thanh ghi) vao: adc_port
; Bien/(thanh ghi) ra:	adc_val
; Bien/(thanh ghi) thay doi: adc_val
; Tac gia: Nguyen Dinh Quoc Dai 20191724
;---------------------------
	;Start of Conversion
	clr adc_cs                ;Make CS low
	clr adc_wr                ;Make WR Low
	nop
	setb adc_wr               ;Make WR High
	setb adc_cs               ;Make CS high
wait:
	jb adc_intr,wait          ;Wait for INTR signal
	;Conversion done
	;Read ADC value
	clr adc_cs                ;Make CS Low
	clr adc_rd                ;Make RD Low
	nop
	mov A,adc_port        ;Read the converted value
	mov adc_val,A         ;Store it in local variable
	setb adc_rd               ;Make RD High
	setb adc_cs               ;Make CS High
	ret                   ;Reading done
Hex2BCD:
; Bien/(thanh ghi) vao: Acc
; Bien/(thanh ghi) ra: BCD_donvi(R0), BCD_chuc(R1), BCD_tram(R2)
; Bien/(thanh ghi) thay doi: BCD_donvi(R0), BCD_chuc(R1), BCD_tram(R2)
; Tac gia: Nguyen Dinh Quoc Dai 20191724
;---------------------------	
	mov BCD_donvi,#00h 	
	mov BCD_chuc,#00h 	
	mov BCD_tram,#00h 
	cjne A,#00h,c1_Hex2BCD 	//If ADC value is not 0 then continue
	ret 	
	c1_Hex2BCD:
	clr C 	
	mov B,#100 	//chia cho 100 de lay hang tram
	div AB 	
	mov BCD_tram,A 	//luu hang tram vao thanh ghi R2 (BCD_tram)
	clr C 	
	mov A,B 	
	mov B,#10 	//chia cho 10 de lay hang chuc
	div AB 	
	mov BCD_chuc,A 	//luu hang chuc vao thanh ghi R1 (BCD_chuc)
	mov BCD_donvi, B 	// luu hang don vi vao thanh ghi R0 (BCD_donvi)
ret
HienThiNhay:
; Bien/(thanh ghi) vao: ... 
; Bien/(thanh ghi) ra: led_7(P0), led_don_vi, led_chuc, led_tram
; Bien/(thanh ghi) thay doi: led_7(P0), led_don_vi, led_chuc, led_tram
; Tac gia: Nguyen Dang Truong 20192129
;---------------------------
	mov dptr,#ma7seg
	clr led_don_vi;enable 7seg no1							//setb error ;enable 7seg no1
	clr led_chuc
	clr led_tram
	mov a,#10
	movc a,@a+dptr
	mov led_7,a 
	lcall delay_nhay 
	setb led_don_vi
	setb led_chuc
	setb led_tram ;disable 7seg no1							//clr error ;disable 7seg no1	
	lcall delay_nhay

	delay_nhay:
		mov r2, #6    

		lap1:
			mov r3, #200

		lap2:
			mov r4, #200
			djnz r4, $
			djnz r3, lap2
			djnz r2, lap1    ; tong thoi gian Delay la: 200*200*12*2us = 0.96s
		ret
ret
HienThi:
; Bien/(thanh ghi) vao: BCD_donvi(R0), BCD_chuc(R1), BCD_tram(R2)
; Bien/(thanh ghi) ra: led_7, led_don_vi, led_chuc, led_tram
; Bien/(thanh ghi) thay doi: led_7, led_don_vi, led_chuc, led_tram
; Tac gia: Nguyen Dang Truong 20192129
;---------------------------
	mov dptr,#ma7seg

	clr led_don_vi ;enable 7seg no1							//setb donvi ;enable 7seg no1
	mov a,BCD_donvi
	movc a,@a+dptr
	mov led_7,a ;so 5
	acall delay ;delay 250us
	setb led_don_vi ;disable 7seg no1							//clr donvi ;disable 7seg no1	


	clr led_chuc												//setb chuc
	mov a,BCD_chuc
	movc a,@a+dptr
	mov led_7,a ;so 7
	acall delay
	setb led_chuc												//clr chuc

	clr led_tram ;enable 7seg no1							//setb donvi ;enable 7seg no1
	mov a,BCD_tram
	movc a,@a+dptr
	mov led_7,a ;so 5
	acall delay ;delay 250us
	setb led_tram ;disable 7seg no1							//clr donvi ;disable 7seg no1
	
	
	delay:
	mov r7,#250
	djnz r7,$
	ret
ret
	ma7seg:
	db 03fh,006h,05bh,04fh,066h,06dh,07dh,007h,07fh,06fh, 079h
;===============================
HienThiDP:
; Bien/(thanh ghi) vao: BCD_donvi(R0), BCD_chuc(R1), BCD_tram(R2)
; Bien/(thanh ghi) ra: led_7, led_don_vi, led_chuc, led_tram
; Bien/(thanh ghi) thay doi: led_7, led_don_vi, led_chuc, led_tram
; Tac gia: Nguyen Dang Truong 20192129
;---------------------------
	mov dptr,#ma7segdp

	clr led_don_vi ;enable 7seg no1							//setb donvi ;enable 7seg no1
	mov a,BCD_donvi
	movc a,@a+dptr
	mov led_7,a ;so 5
	acall delay ;delay 250us
	setb led_don_vi ;disable 7seg no1							//clr donvi ;disable 7seg no1	


	clr led_chuc												//setb chuc
	mov a,BCD_chuc
	movc a,@a+dptr
	mov led_7,a ;so 7
	acall delay
	setb led_chuc												//clr chuc

	clr led_tram ;enable 7seg no1							//setb donvi ;enable 7seg no1
	mov a,BCD_tram
	movc a,@a+dptr
	mov led_7,a ;so 5
	acall delay ;delay 250us
	setb led_tram ;disable 7seg no1							//clr donvi ;disable 7seg no1
	
	

ret
	ma7segdp:
	db 0bfh,086h,0dbh,0cfh,0e6h,0edh,0fdh,087h,0ffh,0efh, 0f9h
;==================================================================
DieuChinhSP:
; Bien/(thanh ghi) vao: temp_thrshld (38H), stopbutton(P3.3), setbutton(P3.2)
; Bien/(thanh ghi) ra: temp_thrshld (38H), led_7, led_don_vi, led_chuc, led_tram
; Bien/(thanh ghi) thay doi: temp_thrshld (38H), led_7, led_don_vi, led_chuc, led_tram
; Tac gia: Nguyen Tuan Anh 20191677
;---------------------------
	mov A, temp_thrshld ;Hien thi nhiet do nguong
	lcall Hex2BCD
	lcall HienThiDP
	jb stopbutton, endDieuChinhSP
	mov A, temp_thrshld
	cjne A, #100, tang_sp
	mov temp_thrshld, #0
	
	mov A, temp_thrshld ;Hien thi nhiet do nguong
	lcall Hex2BCD
	lcall HienThiDP
	
	acall delay_sp
	sjmp endDieuChinhSP
	tang_sp:
		inc temp_thrshld
	acall delay_sp
	
	mov A, temp_thrshld ;Hien thi nhiet do nguong
	lcall Hex2BCD
	lcall HienThiDP
	
	sjmp endDieuChinhSP
	delay_sp:
		mov r2, #6    

		lap1sp:
			mov r3, #200

		lap2sp:
			mov r4, #200
			djnz r4, $
			djnz r3, lap2sp
			djnz r2, lap1sp    ; tong thoi gian Delay la: 200*200*6*2us = 0.48s
		ret
		
	endDieuChinhSP:
ret
;============================================================================================
;Ctrinh_phuc_vu_ngat
;============================================================================================
SetButtonPressed:
	jb over_temp, endext0
	jbc set_mode, endext0
	setb set_mode
	endext0:
reti
;============================================================================================
StopButtonPressed:
	jnb over_temp, endext1
	setb stop_mode
	endext1:
reti
;=============================================================================================
NgatTimer0:
	clr TR0
	inc timer_count
	mov A, timer_count
	cjne A, #014H, endtimer0
	lcall DoNhietDo
	mov timer_count, #000H
	endtimer0:
	mov TL0, #0AFh
	mov TH0, #03Ch
	setb TR0
	reti
end